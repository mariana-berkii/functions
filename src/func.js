const getSum = (str1, str2) => {
  const regex = /([a-z])\w+/gi;
  if (typeof str1 !== 'string' && typeof str2 !== 'string') return false;
  if (str1.match(regex) || str2.match(regex)) return false;

  if (str1 === '') {
    return (parseInt(str2) + 0).toString();
  }
  if (str2 === '') {
    return (parseInt(str1) + 0).toString();
  }

  if (str1.length > 16 || str2.length > 16) {
    let sum = (BigInt(str1) + BigInt(str2)).toString();
    return `${sum.slice(0, 6)}.......${sum.slice(-7)}`;
  }

  return (parseInt(str1) + parseInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countOfPosts = 0, countofComments = 0;

  listOfPosts.forEach(el => {
    if (el.author === authorName) countOfPosts += 1;
    if (el.comments !== undefined) {
      el.comments.forEach(post => {
        if (post.author === authorName) countofComments += 1;
      })
    }
  })

  return `Post:${countOfPosts},comments:${countofComments}`;
};


const tickets = (people) => {
  let queue = [];
  [...people].forEach(el => queue.push(parseInt(el)));

  if (queue[0] > 25) return `NO`;

  let outcome = queue.reduce((total, el) => {
    let change = el - 25;
    if (change <= total) {
      return total + el - change;
    }
    else {
      return total - el;
    }
  })

  if (outcome > 0) {
    return `YES`;
  } else {
    return `NO`;
  }

};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
